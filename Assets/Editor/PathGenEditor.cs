﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PathGenerator))]
public class PathGenEditor : Editor {
    public override void OnInspectorGUI() {
        DrawDefaultInspector();

        var pathGen = target as PathGenerator;
        if (GUILayout.Button("Generate")) {
            pathGen.DestroyPath();
            pathGen.GeneratePath();
            pathGen.GenerateMesh();
        }
    }
}
