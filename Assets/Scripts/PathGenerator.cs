﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathState {
    public Vector3 point;
    public Vector3 forward;
    public Vector3 right;
    public int stepsRight;
    public int stepsUp;
    public PathState nextState = null;
}

public class PathGenerator : MonoBehaviour {
    public float absorbDist = 0.5f;
    public float maxDist = 50f;
    public float forkWeight = 1f;
    public float rotationMin = 30f;
    public float rotationMax = 150f;
    public float biasWeight = 0.25f;
    public float stepDistance = 5f;
    public float rightStepWeight = 1f;
    public float upStepWeight = 1f;

    public float pathWidth = 0.2f;

    private List<PathState> path;
    private delegate float BiasFunction(int stepsRight);

    private float ForkWeight(int count) {
        return Mathf.Sqrt(count) * stepDistance / maxDist * forkWeight;
    }
    
    private float Bias(int stepsRight) {
        return Mathf.Sin(stepsRight);
    }

    public void DestroyPath() {
        path.Clear();
    }

	void Start () {
        path = new List<PathState>();
        GeneratePath();
        GenerateMesh();
	}
     
    public void GenerateMesh() {
        Mesh mesh = new Mesh();

        List<Vector3> vertices = new List<Vector3>();
        List<int> triangles = new List<int>();
        List<Vector3> normals = new List<Vector3>();
        List<Color> colors = new List<Color>();

        foreach (PathState state in path) {
            var startState = state;
            var endState = state.nextState;

            Vector3 start = startState.point;
            Vector3 end = endState.point;

            var col = Random.ColorHSV(0, 1, 0, 1, 0, 1, 1, 1);
            var topRight = end + endState.right * pathWidth;
            var topLeft = end - endState.right * pathWidth;
            var bottomRight = start + startState.right * pathWidth;
            var bottomLeft = start - startState.right * pathWidth;

            // calculate plane normal
            var normal = Vector3.Cross(bottomRight - topRight, bottomRight - bottomLeft).normalized;

            // TODO: remove duplicates
            int topRightIndex = vertices.Count;
            vertices.Add(topRight);
            normals.Add(normal);
            colors.Add(col);

            int topLeftIndex = vertices.Count;
            vertices.Add(topLeft);
            normals.Add(normal);
            colors.Add(col);

            int bottomRightIndex = vertices.Count;
            vertices.Add(bottomRight);
            normals.Add(normal);
            colors.Add(col);

            int bottomLeftIndex = vertices.Count;
            vertices.Add(bottomLeft);
            normals.Add(normal);
            colors.Add(col);

            // top-right triangle
            triangles.Add(bottomLeftIndex);
            triangles.Add(bottomRightIndex);
            triangles.Add(topRightIndex);

            // top-left triangle
            triangles.Add(bottomLeftIndex);
            triangles.Add(topRightIndex);
            triangles.Add(topLeftIndex);
        }

        mesh.SetVertices(vertices);
        mesh.SetColors(colors);
        mesh.triangles = triangles.ToArray();
        mesh.RecalculateNormals();
        mesh.RecalculateTangents();
        mesh.RecalculateBounds();

        GetComponent<MeshFilter>().mesh = mesh;
    }

    public void GeneratePath() {
        GeneratePath(Vector3.zero, Vector3.forward, Vector3.zero);
    }

    private void GeneratePath(Vector3 start, Vector3 forward, Vector3 initial) {
        var state = new PathState() {
            point = start,
            forward = forward,
            right = Vector3.right,
            stepsRight = 0
        };

        int count = 0;
        do {
            path.Add(state);
            var nextState = NextPoint(state, Bias);
            state.nextState = nextState;
            state = nextState;
            ++count;

            // calculate forking
            if (Random.value < ForkWeight(count)) {
                Debug.Log("Branching!");
                Vector3 newStart = state.point;
                Vector3 newForward = state.forward;
                
                Vector3 leftForward = Quaternion.Euler(0, Random.Range(0f, 360f), 0) * newForward;
                Vector3 rightForward = Quaternion.Euler(0, Random.Range(rotationMin, rotationMax), 0) * leftForward;
                GeneratePath(newStart, leftForward, Vector3.zero);
                GeneratePath(newStart, rightForward, Vector3.zero);
                return;
            }
            
            // calculate absorption
            foreach (PathState pathState in path) {
                if (Vector3.Distance(state.point, pathState.point) < absorbDist) {
                    Debug.Log("Absorbing!");
                    return;
                }
            }
        } while (Vector3.Distance(path[count - 1].point, initial) < maxDist);
    }

    private PathState NextPoint(PathState prevState, BiasFunction bias) {
        float stepRight = Random.Range(-1f, 1f) - bias(prevState.stepsRight);

        // check if we took a step right
        if (stepRight > 0) {
            if (prevState.stepsRight > 0) {
                ++prevState.stepsRight;
            } else {
                prevState.stepsRight = 1;
            }
        } else {
            if (prevState.stepsRight > 0) {
                prevState.stepsRight = -1;
            } else {
                --prevState.stepsRight;
            }
        }

        float stepUp = Random.Range(-1f, 1f) - bias(prevState.stepsUp);

        // check if we took a step up
        if (stepUp > 0) {
            if (prevState.stepsUp > 0) {
                ++prevState.stepsUp;
            } else {
                prevState.stepsUp = 1;
            }
        } else {
            if (prevState.stepsUp > 0) {
                prevState.stepsUp = -1;
            } else {
                --prevState.stepsUp;
            }
        }

        Vector3 next = prevState.point + prevState.forward * stepDistance
                                       + prevState.right * stepRight * rightStepWeight
                                       + Vector3.up * stepUp * upStepWeight;
        Vector3 nextForward = (next - prevState.point).normalized;
        Vector3 nextRight = Quaternion.Euler(0, -90, 0) * nextForward;
        return new PathState() {
            point = next,
            forward = nextForward,
            right = nextRight,
            stepsRight = prevState.stepsRight
        };
    }
}
